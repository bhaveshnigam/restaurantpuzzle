import itertools


class Restaurant(object):
  """
  An object representing a restaurant which knows its menu and
  what the cheapest price for a given set of order items is.

  Attributes:
    ID: The restaurant ID
    Menu: The restaurant menu items
  """

  def __init__(self, ID, items):
    self.ID = ID
    self.Menu = items
    # Create a denormalized set of all items for easy satisfaction testing.
    self.Items = set.union(*[set(item['items']) for item in items])

  def Satisfies(self, order):
    """Checks if the restaurant can serve the required order.

    Args:
      order: The requested order.

    Returns:
      True if the restaurant is able to serve the menu else False.
    """
    return self.Items >= set(order)

  def GetItemsFor(self, item):
    """Fetches each item on the menu that include the order item.

    Args:
      item: The desired item which needs to be searched for.

    Returns:
      List of menu items which include the queried item.
    """
    return [x for x in self.Menu if item in x['items']]

  def GetCombinations(self, order):
    """Get different combinations in which the request order could be served as.

    Args:
      order: The requested order.

    Returns:
      List of combinations the order could be served in.
    """
    return itertools.product(*[self.GetItemsFor(item) for item in order])

  def GetBestPrice(self, order):
    """Get the lowest price that includes at least all order items.

    Args:
      order: The requested order.

    Returns:
      The minimum price for the given order
    """
    prices = []
    for combo in self.GetCombinations(order):
      # If some of the items are in the same value meal, that price is just paid once.
      unique_items = set([(m['price'], tuple(m['items'])) for m in combo])
      # Sum the price of each item to get the total.
      total_price = sum(item[0] for item in unique_items)
      prices.append(total_price)
    return min(prices)
