import solver
import unittest
import mock


class CheapestMenuTest(unittest.TestCase):
  """Test cases for solver.CheapestMenu."""

  @classmethod
  def setUpClass(cls):
    fake_data = [
        {'items': ['burger'], 'price': 4.0, 'id': '1'},
        {'items': ['tofu_log'], 'price': 8.0, 'id': '1'},
        {'items': ['burger'], 'price': 5.0, 'id': '2'},
        {'items': ['tofu_log'], 'price': 6.5, 'id': '2'},
        {'items': ['chef_salad'], 'price': 4.0, 'id': '3'},
        {'items': ['burger'], 'price': 3.5, 'id': '3'},
        {'items': ['steak_salad_sandwich'], 'price': 8.0, 'id': '3'},
        {'items': ['steak_salad_sandwich'], 'price': 5.0, 'id': '4'},
        {'items': ['wine_spritzer'], 'price': 2.5, 'id': '4'},
        {'items': ['extreme_fajita'], 'price': 4.0, 'id': '5'},
        {'items': ['fancy_european_water'], 'price': 8.0, 'id': '5'},
        {'items': ['fancy_european_water'], 'price': 5.0, 'id': '6'},
        {'items': ['extreme_fajita', 'jalapeno_poppers', 'extra_salsa'],
         'price': 6.0, 'id': '6'},
        {'items': ['jalapeno_poppers', 'chef_salad'], 'price': 6.5, 'id': '2'},
        {'items': ['chef_salad', 'extra_salsa'], 'price': 4.0, 'id': '3'},
        {'items': ['steak_salad_sandwich', 'burger'], 'price': 8.0, 'id': '3'}
    ]
    cls.mock_fetch_data = mock.MagicMock()
    cls.mock_fetch_data.return_value = fake_data
    solver.FetchData = cls.mock_fetch_data

  @classmethod
  def tearDownClass(cls):
    cls.mock_fetch_data.reset_mock()

  def setUp(self):
    self.test_file_name = ''

  def testSingleOrder(self):
    test_order = ['burger']
    expected_output = '3, 3.5'
    self.assertEquals(expected_output,
                      solver.main(self.test_file_name, test_order))

  def testMultipleOrder(self):
    test_order = ['burger', 'chef_salad']
    expected_output = '3, 7.5'
    self.assertEquals(expected_output,
                      solver.main(self.test_file_name, test_order))

  def testMultipleOrder2(self):
    test_order = ['steak_salad_sandwich', 'chef_salad']
    expected_output = '3, 12.0'
    self.assertEquals(expected_output,
                      solver.main(self.test_file_name, test_order))

  def testMultipleOrder3(self):
    test_order = ['burger', 'tofu_log']
    expected_output = '2, 11.5'
    self.assertEquals(expected_output,
                      solver.main(self.test_file_name, test_order))

  def testMultipleOrder4(self):
    test_order = ['fancy_european_water', 'extreme_fajita']
    expected_output = '6, 11.0'
    self.assertEquals(expected_output,
                      solver.main(self.test_file_name, test_order))

  def testOrderNotAvailable(self):
    test_order = ['chef_salad', 'wine_spritzer']
    expected_output = 'No restaurant found for desired order.'
    self.assertEquals(expected_output,
                      solver.main(self.test_file_name, test_order))


if __name__ == '__main__':
  unittest.main()
