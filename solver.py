#!/usr/bin/env python
import sys, os
import csv
import itertools
from restaurant import Restaurant
import time


def ParseArgs():
  """Parse the Args passed to the script.

  Returns:
    Args passed to the scripts if validates else displays a usage
    instruction.
    Eg: sample_data.csv ['item1', 'item2']
  """
  args = sys.argv[1:] # We don't care about the first argument, this file.
  if len(args) < 2:
    print ("USAGE: %s csv_file item_1 [item_2, item_3]"
           % os.path.split(__file__)[-1])
    sys.exit(1)
  return args[0], args[1:]

def GetRestaurants(csv_data):
  """Fetches a list of Restaurant object for the given CSV data.

  Args:
    csv_data: The CSV data having restaurant ids and its menu.

  Returns:
    List of Restaurant objects.
  """
  # Group the items by restaurant ID.
  restaurant_data = itertools.groupby(csv_data, lambda x: x['id'])
  # Create restaurant objects.
  restaurants = [Restaurant(rest_id, list(group))
                 for rest_id, group in restaurant_data]
  return restaurants


def FetchData(csv_path):
  """Initializes data from the provided CSV filename.

    Args:
      csv_path: The CSV file path to fetch the data from.

    Returns:
      List of dictionary, each row represented as an dictionary.
      eg:
        [{'id' :'1', 'items': 'tofu_log', 'price': 8.0},
         {'id' :'1', 'items': 'burger', 'price': 4.0}]
  """
  csv_descriptor = open(csv_path, 'rb')
  reader = csv.reader(csv_descriptor)
  rows = []
  for row in reader:
    rest_id = row[0].strip()
    price = float(row[1].strip())
    items = [item.strip() for item in row[2:]]
    rows.append({'id': rest_id, 'price': price, 'items': items})
  return rows


def main(csv_path, order):
  """Finds minimum bill of the order desired from the data in csv file.

  Prints restaurant offering the order desired along with the minimum value
  of the bill. Outputs message in case no restaurant with desired order is
  found.

  Args:
    csv_path: The csv file path to read the data from. Path is in reference to
        the directory of this file.
    order: List of items that the user desires to order.
  """
  csv_data = FetchData(csv_path)
  restaurants = GetRestaurants(csv_data)

  # Generate a list of the best price and restaurant ID for each restaurant.
  results = [(rest.GetBestPrice(order), rest.ID)
             for rest in restaurants if rest.Satisfies(order)]

  # If any restaurants had all the desired items, return the lowest-priced result, otherwise 'nil'.
  if results:
    lowest, rest_id = min(results)
    return '%s, %s' % (rest_id, lowest)
  else:
    return 'No restaurant found for desired order.'


if __name__ == "__main__":
  print main(*ParseArgs())
